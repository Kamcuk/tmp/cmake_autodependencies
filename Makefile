all:
	cmake -S. -B _build
	make -C _build VERBOSE=1 --no-print-directory
	cd _build && ctest -V
