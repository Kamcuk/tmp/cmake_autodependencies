#!/bin/sh
grep '#[[:space:]]*include[[:space:]]*' "$1" | sed 's/#[[:space:]]*include[[:space:]]*["<]\(.*\)[>"][[:space:]]*$/\1/' |
	tr '\n' ';' | sed 's/;$//'
