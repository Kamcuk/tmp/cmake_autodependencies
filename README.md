# Example of autoincluding libraries

## to run

Just:

    make

which will do:

    cmake -S. -B _build
    make -B _build
    cd _build && ctest

## How it works:

The hearth of everything is inside CMakeLists.txt

Ok, so we have libraries inside folders `lib1`, `lib2`, `lib3`.

Libraries can have many .h files and .c files. All files should be in the root directory of the projects, nowhere else.
This could be fixed by using some good scripting.

And we have applications: `app1` and `app2`.

Now.

Any .c or any .h file from anywhere can add any .h include from anywhere.

What does cmake do:

- for each file in each project we extract the `#include` files
- we filter from the files the standard libraries headers, ex. stdio.h
- for each such include file
   - we search that file in our source tree
   - when the file is found, we extract the directory from where it is
   - and link against a target that is named the same as the directory

After the linking, the include paths and all the other is done by cmake.
We need to care about only how to find a target with the header file.

The small script `get_includes.sh` or so is there just to get all the `#include` from a file.
I believe it could be even removed and substituted for a smart cmake script with regexes. 

## Extension, ie. TODO:

Ideally this looks like this:

- We have a list of targets (libraries and/or executable)
- Each target has a list of headers and source that are inside it
- We must link target A with target B
  - when any of the files inside target A `#include` any of the files inside target B

This could be very easy done in cmake, because we can `target_link_libraries` after all libraries are visible.

That means we would need just to have list of targets aaand we are ready to go.
We can get the sources of each target with `target_get_property`.
After that it's just pattern matching and maths. And because
cmake allows for cyclic references, it's plain and easy.

# Author

Kamil Cukrwoski 2019

Licensed jointly under MIT License and Beerware License


